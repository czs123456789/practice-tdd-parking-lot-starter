package com.parkinglot;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class ParkingTicket {

    private Car car;

    private boolean isValid;

    public void setValid() {
        isValid = false;
    }

    public boolean getValid() {
        return isValid;
    }

    public ParkingTicket() {
    }

    public ParkingTicket(Car car) {
        this.isValid = true;
        this.car = car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Car getCar() {
        return car;
    }
}
