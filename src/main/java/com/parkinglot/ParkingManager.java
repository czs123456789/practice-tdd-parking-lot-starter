package com.parkinglot;

import java.util.ArrayList;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class ParkingManager{
    private Boy boy;

    public ParkingManager(ParkingLot... parkingLot) {
        this.boy = new ParkingBoy(parkingLot);
    }

    public void setBoy(Boy boy) {
        this.boy = boy;
    }

    public ParkingManager(Boy boy) {
        this.boy = boy;
    }

    public ParkingTicket park(Car car) {
        return boy.park(car);
    }

    public Car fetch(ParkingTicket ticket) {
        return boy.fetch(ticket);
    }
}
