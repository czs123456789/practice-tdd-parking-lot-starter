package com.parkinglot;

public interface Boy {
    public ParkingTicket park(Car car);

    public Car fetch(ParkingTicket ticket);
}
