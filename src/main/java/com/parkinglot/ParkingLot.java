package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class ParkingLot {

    private final int CAPACITY;
    private List<Car> cars = new ArrayList<>();
    private List<ParkingTicket> tickets = new ArrayList<>();

    public ParkingLot() {
        CAPACITY = 10;
    }

    public ParkingLot(int CAPACITY) {
        this.CAPACITY = CAPACITY;
    }

    public ParkingTicket park(Car car) {
        if (!isCapacityMoreZero()) {
            throw new NoAvailablePositionException("No available position");
        }
        this.cars.add(car);
        ParkingTicket ticket = new ParkingTicket(car);
        tickets.add(ticket);
        return ticket;
    }

    public boolean isCapacityMoreZero() {
        return cars.size() < CAPACITY;
    }

    public Car fetch(ParkingTicket ticket) {
        if (!tickets.contains(ticket)) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
        }
        if (!ticket.getValid()) {
            throw new UnrecognizedParkingTicketException("Unrecognized parking ticket");
        }
        ticket.setValid();
        cars.remove(ticket.getCar());
        return ticket.getCar();
    }

    public int getCAPACITY() {
        return CAPACITY-cars.size();
    }

    public int getTotal() {
        return CAPACITY;
    }

    public List<ParkingTicket> getTickets() {
        return tickets;
    }
}
