package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class ParkingBoy implements Boy {

    private List<ParkingLot> parkingLots;

    public ParkingBoy(ParkingLot... parkingLot) {
        this.parkingLots = new ArrayList<>();
        Arrays.stream(parkingLot).forEach(parkingLots::add);
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot p = parkingLots.stream().filter(parkingLot -> parkingLot.isCapacityMoreZero()).findFirst().orElse(parkingLots.get(0));

        return p.park(car);
    }
    @Override
    public Car fetch(ParkingTicket ticket) {
        ParkingLot parkingLot1 = parkingLots.stream().filter(parkingLot -> parkingLot.getTickets().contains(ticket)).findFirst().orElse(parkingLots.get(0));

        return parkingLot1.fetch(ticket);
    }

}
