package com.parkinglot;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class UnrecognizedParkingTicketException extends RuntimeException {

    public UnrecognizedParkingTicketException(String message) {
        super(message);
    }
}
