package com.parkinglot;

import java.util.*;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class SuperSmartParkingBoy implements Boy {

    private List<ParkingLot> parkingLots;

    public SuperSmartParkingBoy(ParkingLot... parkingLot) {
        this.parkingLots = new ArrayList<>();

        Arrays.stream(parkingLot).forEach(parkingLots::add);
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot p = parkingLots.stream()
                .reduce((result,item) -> 1.0*result.getCAPACITY()/result.getTotal() >= 1.0*item.getCAPACITY()/ result.getTotal() ? result : item).get();

        return p.park(car);
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        ParkingLot parkingLot1 = parkingLots.stream().filter(parkingLot -> parkingLot.isCapacityMoreZero()).findFirst().orElse(parkingLots.get(0));

        return parkingLot1.fetch(ticket);
    }

}
