package com.parkinglot;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class NoAvailablePositionException extends RuntimeException {

    public NoAvailablePositionException(String message) {
        super(message);
    }
}
