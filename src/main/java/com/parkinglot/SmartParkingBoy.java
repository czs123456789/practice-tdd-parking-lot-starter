package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class SmartParkingBoy implements Boy {

    private Queue<ParkingLot> parkingLots;

    public SmartParkingBoy(ParkingLot... parkingLot) {
        this.parkingLots = new PriorityQueue<>(new Comparator<ParkingLot>() {
            @Override
            public int compare(ParkingLot o1, ParkingLot o2) {
                return o2.getCAPACITY()- o1.getCAPACITY();
            }
        });

        Arrays.stream(parkingLot).forEach(parkingLots::add);
    }

    @Override
    public ParkingTicket park(Car car) {
        ParkingLot p = parkingLots.stream().filter(parkingLot -> parkingLot.isCapacityMoreZero()).findFirst().orElse(parkingLots.peek());

        return p.park(car);
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        ParkingLot parkingLot1 = parkingLots.stream().filter(parkingLot -> parkingLot.getTickets().contains(ticket)).findFirst().orElse(parkingLots.peek());

        return parkingLot1.fetch(ticket);
    }

}
