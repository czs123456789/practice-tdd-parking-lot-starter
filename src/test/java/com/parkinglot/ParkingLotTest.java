package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Description: test
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class ParkingLotTest {

    @Test
    void should_return_ticket_when_park_given_Parking_lot_and_car() {
    //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
     //when
        ParkingTicket ticket = parkingLot.park(car);

     //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
    //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
     //when
        ParkingTicket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);
     //then
        assertEquals(fetchedCar,car);
    }
    
    @Test
    void should_return_right_car_when_fetch_given_parking_lot_and_tickets() {
    //given
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();
     //when
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);
        //then
        assertEquals(fetchedCar1,car1);
        assertEquals(fetchedCar2,car2);
    }
    
    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_wrong_ticket() {
    //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket wrongTicket = new ParkingTicket();
     //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_car_given_parking_lot_without_capacity() {
    //given
        ParkingLot parkingLot = new ParkingLot(0);

     //when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(new Car()));
        //then
        assertEquals("No available position",exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingTicket Ticket = parkingLot.park(new Car());

        parkingLot.fetch(Ticket);
        //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(Ticket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }
}
