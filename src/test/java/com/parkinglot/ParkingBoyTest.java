package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @Description:
 * @Author: Eisen Chen
 * @CreateDate: 7/14/2023
 **/
public class ParkingBoyTest {

    @Test
    void should_return_ticket_when_park_given_Parking_lot_and_car_and_boy() {
    //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket_and_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);
        //then
        assertEquals(fetchedCar,car);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_and_tickets_and_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car1 = new Car();
        Car car2 = new Car();
        //when
        ParkingTicket ticket1 = parkingBoy.park(car1);
        ParkingTicket ticket2 = parkingBoy.park(car2);
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);
        //then
        assertEquals(fetchedCar1,car1);
        assertEquals(fetchedCar2,car2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_wrong_ticket_and_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket wrongTicket = new ParkingTicket();
        //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongTicket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_car_given_parking_lot_without_capacity_and_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        //when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(new Car()));
        //then
        assertEquals("No available position",exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_used_ticket_and_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket Ticket = parkingBoy.park(new Car());

        parkingBoy.fetch(Ticket);
        //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(Ticket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }

    @Test
    void should_return_first_Parking_lot_when_park_given_two_car_and_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertTrue(parkingLot1.getTickets().contains(ticket));
        assertFalse(parkingLot2.getTickets().contains(ticket));
    }

    @Test
    void should_return_second_Parking_lot_when_park_given_two_car_and_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertFalse(parkingLot1.getTickets().contains(ticket));
        assertTrue(parkingLot2.getTickets().contains(ticket));
    }

    @Test
    void should_return_the_right_card_when_fetch_given_two_ticket_and_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1,parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        //when
        ParkingTicket ticket1 = parkingBoy.park(car1);
        ParkingTicket ticket2 = parkingBoy.park(car2);
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);
        //then
        assertEquals(fetchedCar1,car1);
        assertEquals(fetchedCar2,car2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lots_and_wrong_ticket_and_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket wrongTicket = new ParkingTicket();
        //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongTicket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_park_car_given_parking_lots_without_capacity_and_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);

        //when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(new Car()));
        //then
        assertEquals("No available position",exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lots_and_used_ticket_and_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot,parkingLot1);
        ParkingTicket Ticket = parkingBoy.park(new Car());

        parkingBoy.fetch(Ticket);
        //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(Ticket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }

}
