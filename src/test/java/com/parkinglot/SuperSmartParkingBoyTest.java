package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SuperSmartParkingBoyTest {

    @Test
    void should_return_first_Parking_lot_when_park_given_two_car_and_super_smart_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertTrue(parkingLot1.getTickets().contains(ticket));
        assertFalse(parkingLot2.getTickets().contains(ticket));
    }

    @Test
    void should_return_second_Parking_lot_when_park_given_two_car_and_super_smart_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(9);
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot1,parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertFalse(parkingLot1.getTickets().contains(ticket));
        assertTrue(parkingLot2.getTickets().contains(ticket));
    }

    @Test
    void should_throw_exception_with_error_message_when_park_car_given_parking_lots_without_capacity_and_super_smart_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingLot parkingLot1 = new ParkingLot(0);
        SuperSmartParkingBoy parkingBoy = new SuperSmartParkingBoy(parkingLot,parkingLot1);

        //when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(new Car()));
        //then
        assertEquals("No available position",exception.getMessage());
    }

}