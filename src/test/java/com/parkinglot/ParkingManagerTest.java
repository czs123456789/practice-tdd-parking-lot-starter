package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ParkingManagerTest {

    @Test
    void should_return_result_when_park_or_fetch_given_Parking_lot_and_manager() {
    //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
     //when
        should_return_ticket_when_park_given_Parking_lot_and_car_and_manager(parkingBoy);
        should_return_car_when_fetch_given_parking_lot_and_ticket_and_manager(parkingBoy);
        should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_wrong_ticket_and_manager(parkingBoy);
        should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_used_ticket_and_manager(parkingBoy);

        //given
        ParkingLot parkingLot1 = new ParkingLot();
        SmartParkingBoy smartparkingBoy = new SmartParkingBoy(parkingLot1);
        //when
        should_return_ticket_when_park_given_Parking_lot_and_car_and_manager(smartparkingBoy);
        should_return_car_when_fetch_given_parking_lot_and_ticket_and_manager(smartparkingBoy);
        should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_wrong_ticket_and_manager(smartparkingBoy);
        should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_used_ticket_and_manager(smartparkingBoy);

        //given
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot2);
        //when
        should_return_ticket_when_park_given_Parking_lot_and_car_and_manager(superSmartParkingBoy);
        should_return_car_when_fetch_given_parking_lot_and_ticket_and_manager(superSmartParkingBoy);
        should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_wrong_ticket_and_manager(superSmartParkingBoy);
        should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_used_ticket_and_manager(superSmartParkingBoy);


        ParkingLot parkingLot3 = new ParkingLot(0);
        ParkingBoy parkingBoy1 = new ParkingBoy(parkingLot3);
        should_throw_exception_with_error_message_when_park_car_given_parking_lot_without_capacity_and_manager(parkingBoy1);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot3);
        should_throw_exception_with_error_message_when_park_car_given_parking_lot_without_capacity_and_manager(smartParkingBoy);
        SuperSmartParkingBoy superSmartParkingBoy1 = new SuperSmartParkingBoy(parkingLot3);
        should_throw_exception_with_error_message_when_park_car_given_parking_lot_without_capacity_and_manager(superSmartParkingBoy1);
    }

    void should_return_ticket_when_park_given_Parking_lot_and_car_and_manager(Boy boy) {
        //given

        ParkingManager manager = new ParkingManager(boy);
        Car car = new Car();
        //when
        ParkingTicket ticket = manager.park(car);

        //then
        assertNotNull(ticket);
    }

    void should_return_car_when_fetch_given_parking_lot_and_ticket_and_manager(Boy boy) {

        ParkingManager manager = new ParkingManager(boy);
        Car car = new Car();
        //when
        ParkingTicket ticket = manager.park(car);
        Car fetchedCar = manager.fetch(ticket);

        //then
        assertEquals(fetchedCar,car);
    }


    @Test
    void should_return_ticket_when_park_given_Parking_lot_and_car_and_manager_self() {
        //given

        ParkingManager manager = new ParkingManager(new ParkingLot());
        Car car = new Car();
        //when
        ParkingTicket ticket = manager.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket_and_manager_self() {

        ParkingManager manager = new ParkingManager(new ParkingLot());
        Car car = new Car();
        //when
        ParkingTicket ticket = manager.park(car);
        Car fetchedCar = manager.fetch(ticket);

        //then
        assertEquals(fetchedCar,car);
    }



    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_wrong_ticket_and_manager(Boy boy) {
        //given
        ParkingManager manager = new ParkingManager(boy);
        ParkingTicket wrongTicket = new ParkingTicket();
        //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> manager.fetch(wrongTicket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }

    void should_throw_exception_with_error_message_when_park_car_given_parking_lot_without_capacity_and_manager(Boy boy) {
        //given
        ParkingManager manager = new ParkingManager(boy);
        //when
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> manager.park(new Car()));
        //then
        assertEquals("No available position",exception.getMessage());
    }


    void should_throw_exception_with_error_message_when_fetch_car_given_parking_lot_and_used_ticket_and_manager(Boy boy) {
        //given
        ParkingManager manager = new ParkingManager(boy);
        ParkingTicket Ticket = manager.park(new Car());

        manager.fetch(Ticket);
        //when
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> manager.fetch(Ticket));
        //then
        assertEquals("Unrecognized parking ticket",exception.getMessage());
    }
}