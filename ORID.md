Objective: I have learned advanced TDD and advanced OOP knowledge. At the same time, we did a lot of practice about these.

Reflective:  Fulfilled.

Interpretive: A more reasonable project structure with knowledge of advanced TDD and advanced OOP.

Decisional: I would like to reiterate: We can use TDD to reduce program bugs and achieve program accuracy and robustness. The development process is more organized and purposeful allowing for minimal module development and able to test the functionality of the code in a timely manner. OOP allows us to design a more reasonable project structure.